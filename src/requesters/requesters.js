import {
	BACKEND_URL,
	DEFAULT_REF,
	CATEGORY_ILLUS_NAMED,
	ARTICLE_ENDS_WITH,
	CDN_IMAGE_URL,
	SHOP_FILE_PATH,
	CATEGORY_FILE_INFO_PATH
	// @ts-ignore
} from '$contants/backend.constants';
// @ts-ignore
import { Category } from '$models/category';
// @ts-ignore
import { Article } from '$models/article';
// @ts-ignore
import { ShopItem } from '$models/shop_item';

const DEFAULT_PARAMS = { ref: DEFAULT_REF, fetchWith: fetch };

/**
 * Retrieve all shop items
 * @param {{ref: String, fetchWith: Function}} params
 * @returns {Promise<{status: Number, body: Array<ShopItem>}>}
 */
export async function getShopsItems(params) {
	const { ref, fetchWith } = { ...DEFAULT_PARAMS, ...params };
	const rep_raw = await fetchWith(
		`/api/files/${encodeURIComponent(SHOP_FILE_PATH)}/raw?ref=${ref}`
	);
	const rep_json = await rep_raw.json();

	return {
		status: rep_raw.status,
		body: rep_json.map((/** @type {any} */ r_j) => {
			return new ShopItem(r_j);
		})
	};
}

/**
 * Retrieve all articles for one category_path (Doesn't fetch articles raw_content)
 * @param {String} category_path
 * @param {{ref: String, fetchWith: Function}} params
 * @returns
 */
export async function getArticlesOfCategory(category_path, params) {
	const { ref, fetchWith } = { ...DEFAULT_PARAMS, ...params };
	const rep = await fetchWith(
		`/api/files/${encodeURIComponent(`${category_path}/${CATEGORY_FILE_INFO_PATH}`)}/raw?ref=${ref}`
	);
	if (rep.status != 200) {
		return {
			status: rep.status,
			body: {}
		};
	}
	const rep_json = await rep.json();
	rep_json['articles'] = rep_json['articles'].map(
		(/** @type {{String: any}} */ article) => new Article(article)
	);
	const category = new Category(rep_json);

	return {
		status: rep.status,
		body: category
	};
}

/**
 * Retrieve all categories as a Gitlab API object
 *
 * @param {{ref: String, fetchWith: Function}} params
 */
export async function getCategoriesPath(params) {
	const { ref, fetchWith } = { ...DEFAULT_PARAMS, ...params };

	const rep = await fetchWith(`/api/tree?ref=${ref}`);
	if (rep.status != 200) {
		return {
			status: rep.status,
			body: []
		};
	}
	const files = await rep.json();
	const categories_tree = files.filter(
		(/** @type {{ [x: string]: string; }} */ file) => file['type'] == 'tree'
	);

	return {
		status: rep.status,
		body: categories_tree
	};
}

/**
 * Retrieve all categories
 * @param {{ref: String, fetchWith: Function}} params
 */
export async function getAllCategories(params) {
	const { status, body } = await getCategoriesPath(params);
	if (status != 200) {
		return {
			status: status,
			body: []
		};
	}

	const reps_p = body.map((/** @type {{ [x: string]: string; }} */ { path }) =>
		getArticlesOfCategory(path, params)
	);

	const reps_j = await Promise.all(reps_p);
	const not_200 = reps_j.find((el) => el.status != 200);
	if (not_200) {
		return {
			status: not_200,
			body: []
		};
	}
	return {
		status: 200,
		body: reps_j.map((rep) => rep.body)
	};
}

/**
 * Retrieve all articles of all categories
 * @param {{ref: String, fetchWith: Function}} params
 */
export async function getAllArticlesOfAllCategories(params) {
	const { status, body } = await getAllCategories(params);

	return {
		status: status,
		body: body.map((rep) => rep.articles).flat()
	};
}

/**
 * Retrieve an article from a category_path and the article_path
 * @param {String} category_path
 * @param {String} article_path
 * @param {{ref: String, fetchWith: Function}} params
 * @returns
 */
export async function getArticle(category_path, article_path, params) {
	const { ref, fetchWith } = { ...DEFAULT_PARAMS, ...params };
	const rep = getArticlesOfCategory(category_path, params);
	const rep_raw = fetchWith(
		`/api/files/${encodeURIComponent(
			`${category_path}/${article_path}${ARTICLE_ENDS_WITH}`
		)}/raw?ref=${ref}`
	);
	const reps = await Promise.all([rep, rep_raw]);
	const files = await Promise.all([reps[0].body, reps[1].text()]);

	const article = reps[0].body.articles.find(
		(/** @type {{ path: string; }} */ article) =>
			article.path == `${category_path}/${article_path}${ARTICLE_ENDS_WITH}`
	);
	if (!article) {
		return {
			status: 404,
			body: {}
		};
	}
	article.content_raw = files[1];
	return {
		status: reps[1].status,
		body: article
	};
}
