// @ts-ignore
import { Article } from './article';

export class Category {
	/**
	 * Create a category
	 *
	 * @param {{id: String, title: String, image_src: String, path: String, articles: Array<Article>}} params
	 */
	constructor({ id, title, image_src, path, articles }) {
		this.id = id;
		this.title = title;
		this.image_src = image_src || '';
		this.path = path;
		this.articles = articles;
	}

	get front_path() {
		return `/categories/${encodeURIComponent(this.path)}/`;
	}
}
