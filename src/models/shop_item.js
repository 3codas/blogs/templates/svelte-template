export class ShopItem {
	/**
	 * Create a shop item from elements
	 *
	 * @param {{id: String, title: String, price: Number, image_src: String, description: String, link: String}} params
	 */
	constructor({ id, title, price, image_src, description, link }) {
		this.id = id;
		this.title = title;
		this.price = price;
		this.image_src = image_src;
		this.description = description;
		this.link = link;
	}

	get front_path() {
		return `/shop/${this.id}`;
	}
}
