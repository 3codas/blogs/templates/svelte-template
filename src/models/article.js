// @ts-ignore
import { ARTICLE_ENDS_WITH } from '$contants/backend.constants';

export class Article {
	/**
	 * Create an article
	 *
	 * @param {{content_raw: String, title: String, id: String, path: String, image_src: String, author: String, description: String, publish_at: String}} params
	 */
	constructor({ content_raw, id, title, path, image_src, author, description, publish_at }) {
		this.content_raw = content_raw;
		this.id = id;
		this.title = title;
		this.path = path;
		this.image_src = image_src || '';
		this.author = author;
		this.description = description;
		this.publist_at = publish_at;
	}

	get front_path() {
		return `/categories/${this.path.replace(ARTICLE_ENDS_WITH, '')}`;
	}
}
