import * as renderComponents from '.';

export const renderers = Object.fromEntries(
	Object.entries(renderComponents).map(([key, value]) => [key.toLowerCase(), value])
);
