export const BACKEND_URL = import.meta.env.VITE_BACKEND_URL;
export const DEFAULT_REF = import.meta.env.VITE_DEFAULT_REF;
export const CATEGORY_ILLUS_NAMED = import.meta.env.VITE_CATEGORY_ILLUS_NAMED;
export const ARTICLE_ENDS_WITH = import.meta.env.VITE_ARTICLE_ENDS_WITH;
export const CDN_IMAGE_URL = import.meta.env.VITE_CDN_IMAGE_URL;
export const SHOP_FILE_PATH = import.meta.env.VITE_SHOP_FILE_PATH;
export const CATEGORY_FILE_INFO_PATH = import.meta.env.VITE_CATEGORY_FILE_INFO_PATH;
export const CACHE_TTL = 5 * 60 * 1000; // MINUTES * SECONDS * MILLI
export const CACHE_NAMESPACE = 'gitlab-cache';
