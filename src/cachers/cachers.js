import {
	CACHE_NAMESPACE
	// @ts-ignore
} from '$contants/backend.constants';

import Keyv from '@keyvhq/core';
export const endpointCache = new Keyv({ namespace: CACHE_NAMESPACE });
