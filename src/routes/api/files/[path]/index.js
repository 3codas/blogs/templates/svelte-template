import {
	BACKEND_URL,
	DEFAULT_REF,
	CACHE_TTL
	// @ts-ignore
} from '$contants/backend.constants';

// @ts-ignore
import { endpointCache } from '$cachers/cachers';

const KEY = 'files/';
// @ts-ignore
export async function get({ request, params }) {
	const ref = new URL(request.url).searchParams.get('ref') || DEFAULT_REF;

	const key = `${KEY}/${params.path}/&ref=${ref}`;
	const cache = await endpointCache.get(key);

	if (cache) {
		console.info('Retrieve from cache...');
		return {
			status: 200,
			body: cache
		};
	}
	console.info('Retrieve from Gitlab');
	const rep = await fetch(`${BACKEND_URL}/repository/files/${params.path}?ref=${ref}`);

	if (!rep.ok) {
		return {
			status: rep.status,
			body: []
		};
	}

	const body = await rep.json();
	endpointCache.set(key, body, CACHE_TTL);
	return {
		status: rep.status,
		body: body
	};
}
