import {
	BACKEND_URL,
	DEFAULT_REF,
	CACHE_TTL
	// @ts-ignore
} from '$contants/backend.constants';
// @ts-ignore
import { endpointCache } from '$cachers/cachers';

const KEY = 'files/';
// @ts-ignore
export async function get({ request, params }) {
	const ref = new URL(request.url).searchParams.get('ref') || DEFAULT_REF;

	const key = `${KEY}/${params.path}/raw/&ref=${ref}`;
	const cache = await endpointCache.get(key);

	if (cache) {
		console.info('Retrieve from cache...');
		return {
			status: 200,
			body: cache
		};
	}
	console.info('Retrieve from Gitlab');
	const rep = await fetch(
		`${BACKEND_URL}/repository/files/${encodeURIComponent(params.path)}/raw?ref=${ref}`
	);

	if (!rep.ok) {
		return {
			status: rep.status,
			body: []
		};
	}

	let body;
	try {
		if (rep.headers.get('content-type').includes('text/plain')) {
			body = await rep.text();
		} else {
			body = await rep.json();
		}
	} catch (e) {
		console.error(e);
		body = {};
	}
	endpointCache.set(key, body, CACHE_TTL);
	return {
		status: rep.status,
		body: body
	};
}
