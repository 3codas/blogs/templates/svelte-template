const default_theme = require('tailwindcss/defaultTheme');

/** @type {import('tailwindcss').Config} */
module.exports = {
	content: ['./src/**/*.{html,js,svelte,ts}'],
	theme: {
		extend: {
			fontFamily: {
				sans: ['Mallory', 'Inter var', ...default_theme.fontFamily.sans]
			},
			colors: {
				primary: '#20201e',
				secondary: '#e7003d',
				background: '#fffefe'
			}
		}
	},
	plugins: []
};
